from cmd import Cmd
import requests
import re
import json

class Prompt(Cmd):
    def __init__(self):
        Cmd.__init__(self)
        self.session = requests.session()
        self.url = ""
        # pattern to assure the url format is correct
        self.urlpattern = re.compile("^([0-9]|[a-zA-Z])+(\.pythonanywhere\.com){1}$")

    prompt = 'News Aggregator >> '
    intro = '\nNews aggregator client for comp3011 web services.\nType \'help\' or \'?\' to list commands\n'


    def do_login(self, inp):
        '''Use \'login <url>\' to log into the given web service.'''
        if inp == "":
            print("Please provide a URL!")
            return
        # turn off for testing on localhost
        #check if its a valid pythonanywhere url
        if not bool(self.urlpattern.match(inp)):
            print("Incorrect format, please provide a url in the format \'*.pythonanywhere.com\'.")
            return

        # get user input
        username = input('Username: ')
        password = input('Password: ')
        self.url = inp

        # attempt to send request with url but if its invalid, reset
        try:
            r = self.session.post('http://'+self.url+'/api/login/', data={'username':username, 'password':password})
            print(r.text)
        except:
            print("Invalid URL")
            # make sure url is not stored if a problem occurs
            self.url = ""
        return

    def do_logout(self, inp):
        '''Use \'logout\' to log out of the web service.'''
        # check if logged in client side
        if self.url == "":
            print("You are already logged out!")
            return
        # send request
        r = self.session.post("http://"+self.url+"/api/logout/")
        print(r.text)
        self.url = ""
        return

    def do_post(self, inp):
        '''Used to post a story to the new agency logged in to.'''
        # check if logged in client side (does this server side too, depends on client implementation)
        if self.url == "":
            print("You need to be logged in!")
            return
        # get user input
        headline = input("Headline: ")
        category = input("Category: ")
        region = input("Region: ")
        details = input("Story Details:\n")

        # create payload and send request
        payload = {'headline':headline,'category':category,'region':region,'details':details}
        r = self.session.post('http://'+self.url+'/api/poststory/', json=payload)
        print(r.text)
        return

    def do_list(self, inp):
        '''Lists all news services contained in the directory api.'''
        r = self.session.get('http://directory.pythonanywhere.com/api/list/')
        data = r.json()
        for agency in data['agency_list']:
             print(f"Agency: {agency['agency_name']}\nWebsite: {agency['url']}\nID: {agency['agency_code']}\n")
        return

    def do_news(self, inp):
        '''Displays aggregated news from all of the available services in the directory.

        \'news [-id=][-cat=][-reg=][-date=]\'

        where [-id], [cat], [reg], and [date] are optional switches that have
        the following effects:

        - id: the three-letter id of the news service, for example -id=”JAD”.
          If this switch is left out, the application should collect news from
          all services.
        - cat: the news category, for example -cat=”tech”. If this switch is
          left out the application should assume that cat=”*”.
        - reg: the region of the required stories, for example -reg=”uk”. If
          this switch is left out the application should assume that reg=”*”.
        - date: the date at or after which a story has happened, for example,
          -date=”12/2/2019”. The date should be in following format dd/mm/yyyy.
          If this switch is left out the application should assume that date=”*”.'''

        # retrieve list of agencies from directory
        directoryResponse = self.session.get("http://directory.pythonanywhere.com/api/list/")
        agency_list = directoryResponse.json()['agency_list']

        # defaults to "*" if a match is not found
        idr = re.search('-id=(\S+)', inp)
        id = idr.group(1) if idr is not None else "*"
        catr = re.search('-cat=(\S+)', inp)
        cat = catr.group(1) if catr is not None else "*"
        regr = re.search('-reg=(\S+)', inp)
        reg = regr.group(1) if regr is not None else "*"
        dater = re.search('-date=(\S+)', inp)
        date = dater.group(1) if dater is not None else "*"

        # create payload
        payload = {'story_cat':cat, 'story_region':reg, 'story_date':date}

        if id != "*":
            # code for a selected agency
            selected = None
            # search list for specific ID
            for agency in agency_list:
                if agency['agency_code'] == id:
                    selected = agency
                    break;
            if selected == None:
                print("Agency does not exist!")
                return
            print("Selected Agency: " + selected['agency_name'])
            agency_list = [selected]
        else:
            # code for "*"
            print("Selected Agency: All")

        for agency in agency_list:
            # asure all urls are consistent by stripping end '/' off them
            # ideally would do this for the http:// too in case people hadn't
            # included it
            url = agency['url'].strip('/')
            r = self.session.get(url+"/api/getstories/", json=payload)
            if r.status_code == 404:
                print(f"\nCould find stories from {agency['agency_name']}.\n")
                continue

            print("===============================================================================")
            print(agency['agency_name'])
            print("===============================================================================\n")
            try:
                for story in r.json()['stories']:
                    print(f"ID: {story['key']} | Headline: {story['headline']}\n",
                          f"Category: {story['story_cat']} | Region: {story['story_region']}\n",
                          f"Author: {story['author']}\n",
                          f"Details:\n{story['story_details']}\n",
                          f"{story['story_date']}\n")
                    print("-------------------------------------------------------------------------------\n")

            except:
                print("Could not read json....\n")
        return



    def do_delete(self, inp):
        '''Allows deletion of a story provided a key. \'delete <story_key>\'.'''
        # check if logged in client side
        if self.url == "":
            print("You need to be logged in!")
            return
        # make sure key is integer (though ideally this would be checked server side anyway as mine is)
        try:
            key = int(inp)
        except ValueError as e:
            print("Not a valid key. Enter an integer!")
            return
        # create payload and send request
        payload = {'story_key', key}
        r = self.session.get(self.url, json=payload)
        print(r.text)

    def do_exit(self, inp):
        '''Exits the program and logs out.'''
        # if the user is logged in, log them out
        if self.url != "":
            self.do_logout(inp)
        print("Goodbye!")
        return True

    # if the user presses Ctrl^D
    do_EOF = do_exit

def main():
    Prompt().cmdloop()

if __name__ == '__main__':
    main()
