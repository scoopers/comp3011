from django.db import models

# Create your models here.
class Story(models.Model):
    categories = [('pol','Political News'),('art','Art News'),('tech','Technology News'),('trivia','Trivia News')]
    regions = [('uk','United Kingdom'),('w','World'),('eu','Europe')]
    headline = models.CharField(max_length=64)
    story_cat = models.CharField(max_length=3,choices=categories)
    story_region = models.CharField(max_length=2, choices=regions)
    author = models.ForeignKey('Author',on_delete=models.CASCADE)
    story_date = models.DateField(auto_now_add=True)
    content = models.TextField(max_length=512)

class Author(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    username = models.CharField(max_length=64,unique=True)
    password = models.CharField(max_length=128)
