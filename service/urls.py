from django.urls import path
from . import views

urlpatterns = [
    path('api/login/', views.login),
    path('api/logout/', views.logout),
    path('api/poststory/', views.post_story),
    path('api/getstories/', views.get_stories),
    path('api/deletestory/', views.delete_story)
]
