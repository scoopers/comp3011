from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from service.models import Author, Story
import json
import re


@csrf_exempt
def login(request):
    # forcing correct method and content type
    if request.method != 'POST':
        return HttpResponse('This service only accepts POST requests.', content_type="text/plain", status=400)
    if request.content_type != 'application/x-www-form-urlencoded':
        return HttpResponse('This service only accepts requests of type \'application/x-www-form-urlencoded\'.', content_type="text/plain", status=400)
    # check if logged in
    if request.session._session:
        return HttpResponse('Already logged in!.', content_type="text/plain", status=403)
    # get username and password in request payload
    username = request.POST['username']
    password = request.POST['password']

    # authenticate
    try:
        # find username
        auth = Author.objects.get(username=username)
        # if password matches (could be more secure but hey)
        if auth.password == password:
            request.session.create()
            request.session['authentication'] = auth.id
            # welcome message
            return HttpResponse('Welcome '+auth.first_name+"!" , content_type="text/plain", status=200)
        # else return response
        else:
            return HttpResponse('Incorrect password.', content_type="text/plain", status=403)
    # if no user return response
    except ObjectDoesNotExist:
        return HttpResponse('User does not exist.', content_type="text/plain", status=403)

@csrf_exempt
def logout(request):
    if request.method != 'POST':
        return HttpResponse('This service only accepts POST requests.')
    # check if session exists
    if request.session._session:
        # get rid of session for good
        request.session.flush();
        # goodbye message
        return HttpResponse('Logged out.', content_type="text/plain", status=200)
    else:
        return HttpResponse('Already logged out!', content_type="text/plain", status=403)

@csrf_exempt
def post_story(request):
    # check method and content type
    if request.method != 'POST':
        return HttpResponse('This service only accepts POST requests.', content_type="text/plain", status=400)
    if request.content_type != 'application/json':
        return HttpResponse('This service only accepts requests of type \'application/json\'.', content_type="text/plain", status=400)

    # check if authenticated
    if 'authentication' in request.session:
        authID = request.session['authentication']
        # get id from session to get corresponding author object
        author = Author.objects.get(pk=authID)
        # decode json payload
        body = json.loads(request.body.decode("utf-8"))
        # check json payload contains correct details(keys)
        if not all(i in body for i in ('headline','category','region','details')):
            return HttpResponse('Need all fields in json payload.', content_type="text/plain", status=503)
        # assign
        headline=body['headline']
        category=body['category']
        region=body['region']
        details=body['details']

        # forcing category and region to be any of these options
        catBool = False
        for cat in ['pol','art','tech','trivia']:
            if category == cat:
                catBool = True
        if not catBool:
            return HttpResponse('Category needs to be \'pol,art,tech,trivia\'.', content_type="text/plain", status=503)

        regBool = False
        for reg in ['uk','w','eu']:
            if region == reg:
                regBool = True
        if not regBool:
            return HttpResponse('Region needs to be \'uk,w,eu\'.', content_type="text/plain", status=503)

        # attempt to create story, if a problem occurs return response
        try:
            story = Story(headline=headline, story_cat=category, story_region=region, content=details, author=author)
            story.save()
            return HttpResponse('Story created!', content_type="text/plain", status=201)
        except:
            return HttpResponse('Story could not be created.', content_type="text/plain", status=503)

    else:
        return HttpResponse('Need to be logged in!', content_type="text/plain", status=503)

def get_stories(request):
    # method and content type
    if request.method != 'GET':
        return HttpResponse('This service only accepts GET requests.', content_type="text/plain", status=400)
    if request.content_type != 'application/json':
        return HttpResponse('This service only accepts requests of type \'application/json\'.', content_type="text/plain", status=400)
    # decode json payload
    body = json.loads(request.body.decode("utf-8"))

    # check correct payload keys
    if not all(i in body for i in ('story_region','story_cat','story_date')):
        return HttpResponse('Need all fields in json payload.', content_type="text/plain", status=503)

    # use body as arguments for filtering, remove the wildcard if they occur as they
    # will cause an error in the filtering
    if body['story_cat'] == "*":
        del body['story_cat']
    if body['story_region'] == "*":
        del body['story_region']
    if body['story_date'] == "*":
        del body['story_date']
    else:
        # split the day month and year and create new arguments for filtering
        d, m, y = re.split('/|-' , body['story_date'])
        del body['story_date']
        # __year/month/day is the postfix to access DateField members
        body['story_date__year'] = y
        body['story_date__month'] = m
        body['story_date__day'] = d

    try:
        # get filtered objects
        stories = Story.objects.filter(**body)
    except:
        # if some problem occured
        return HttpResponse('Could not process your request, assure correct format.', content_type='text/plain', status=400)

    # if no stories were found
    if not len(stories):
        return HttpResponse('There are no stories available', content_type="text/plain", status=404)

    # construct payload
    data = [dict(key=story.pk,
                 headline=story.headline,
                 story_cat=story.story_cat,
                 story_region=story.story_region,
                 author=story.author.first_name+" "+story.author.last_name,
                 story_date=str(story.story_date),
                 story_details=story.content) for story in stories]
    payload = {'stories': data}
    # return json payload
    return JsonResponse(payload, content_type="application/json", status=200)

@csrf_exempt
def delete_story(request):
    if request.method != 'POST':
        return HttpResponse('This service only accepts POST requests.', content_type="text/plain", status=400)
    if request.content_type != 'application/json':
        return HttpResponse('This service only accepts requests of type \'application/json\'.', content_type="text/plain", status=400)

    # check logged in
    if not 'authentication' in request.session:
        return HttpResponse('Need to be logged in!', content_type="text/plain", status=403)
    # decode payload
    body = json.loads(request.body.decode("utf-8"))
    # if there isnt a key specified in payload
    if 'story_key' not in body:
        return HttpResponse('Need to specify story key in json payload.', content_type="text/plain", status=503)
    key = body['story_key']

    try:
        # get story object using key and delete
        story = Story.objects.get(pk=key)
        story.delete()
        # return CREATED message
        return HttpResponse('Deleted story successfully', content_type="text/plain", status=201)
    except:
        # return reponse if problem occurs (probably if key doesnt exist)
        return HttpResponse('Could not delete story. Try another key?', content_type="text/plain", status=503)
